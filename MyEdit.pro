#-------------------------------------------------
#
# Project created by QtCreator 2017-03-30T11:09:59
#
#-------------------------------------------------

QT          += core gui
QT          += testlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MyEdit
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    about.cpp

HEADERS  += mainwindow.h \
    about.h

FORMS    += mainwindow.ui \
    about.ui

RESOURCES += \
    icon.qrc \
    movies.qrc
