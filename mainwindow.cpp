#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);





    this->setWindowTitle("untitled.txt");

    //文件 菜单
    //新建文件--->newFileSlot();
    QObject::connect(ui->new_untitle_action,SIGNAL(triggered()),this,SLOT(newUntitleFileSlot()));
    //打开文件---->openFileSlot();
    QObject::connect(ui->open_action,SIGNAL(triggered()),this,SLOT(openFileSlot()));
    //另存文件--->saveAsFileSlot();
    QObject::connect(ui->saveas_action,SIGNAL(triggered()),this,SLOT(saveAsFileSlot()));
    //保存文件--->saveFileSlot();
    QObject::connect(ui->save_action,SIGNAL(triggered()),this,SLOT(saveFileSlot()));
    //关闭程序--->close(); 系统提供的close();
    QObject::connect(ui->exit_action,SIGNAL(triggered()),this,SLOT(close()));

    //编辑菜单
    //撤销
    QObject::connect(ui->undo_action,SIGNAL(triggered()),ui->textEdit,SLOT(undo()));
    //重做
    QObject::connect(ui->redo_action,SIGNAL(triggered()),ui->textEdit,SLOT(redo()));
    //剪切
    QObject::connect(ui->cut_action,SIGNAL(triggered()),ui->textEdit,SLOT(cut()));
    //复制
    QObject::connect(ui->copy_action,SIGNAL(triggered()),ui->textEdit,SLOT(copy()));
    //粘贴
    QObject::connect(ui->paste_action,SIGNAL(triggered()),ui->textEdit,SLOT(paste()));
    //选择全部
    QObject::connect(ui->select_all_action,SIGNAL(triggered()),ui->textEdit,SLOT(selectAll()));
    //设置字体--->setFontSlot();
    QObject::connect(ui->font_action,SIGNAL(triggered()),this,SLOT(setFontSlot()));
    //设置字体--->setColorSlot();
    QObject::connect(ui->color_action,SIGNAL(triggered()),this,SLOT(setColorSlot()));
    //插入系统当前时间--->currentDateTimeSlot();
    QObject::connect(ui->current_time_action,SIGNAL(triggered()),this,SLOT(currentDateTimeSlot()));


    //帮助菜单

    //关于我
    QObject::connect(ui->about_me_action,SIGNAL(triggered()),this,SLOT(aboutMeSlot()));
    //关于QT
    QObject::connect(ui->about_qt_action,SIGNAL(triggered()),qApp,SLOT(aboutQt()));
    //关于程序
    QObject::connect(ui->about_program_action,SIGNAL(triggered()),this,SLOT(aboutProgramSlot()));

}

MainWindow::~MainWindow()
{
    delete ui;
}
//新建一个文件
void MainWindow::newUntitleFileSlot()
{
    if(ui->textEdit->document()->isModified())
    {
        QMessageBox msgBox;
        msgBox.setText("文本已经被编辑.");
        msgBox.setInformativeText("想要去保存吗?");
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Save);
        int ret = msgBox.exec();
        switch (ret) {
          case QMessageBox::Save:
              this->saveFileSlot();
              break;
          case QMessageBox::Discard:
              ui->textEdit->clear();
              break;
          case QMessageBox::Cancel:
              //event->ignore();
              break;
          default:
              //event->ignore();
              break;
        }

    }
    else
    {

        ui->textEdit->clear();
        this->setWindowTitle("untitled.txt  -- MyEdit");
    }
}

//打开一个文件
void MainWindow::openFileSlot()
{
    //get filename
    QString filename = QFileDialog::getOpenFileName(this,"打开文件",QDir::currentPath());
    saveFileName = filename;
    //qDebug() << "文件名:" << filename;
    if(filename.isEmpty())
    {
        QMessageBox::information(this,"打开错误","请选择一个纯文本文件");
        return ;
    }
    QFile *file = new QFile();
    // set file name
    file->setFileName(filename);
    //open file as read only
    bool ok = file->open(QIODevice::ReadOnly);
    if(ok)
    {
        //关联文件和流
        QTextStream in(file);
        //read all the text in file
        QString text=in.readAll();
        ui->textEdit->setText(text);
        file->close();

        //QFile属于QTCore模块,所以需要自己清楚内存.
        delete file;
    }
    else
    {
        QMessageBox::information(this,"打开文件错误","打开失败:"+file->errorString());
        return ;
    }
}
//另存一个文件
void MainWindow::saveAsFileSlot()
{
    QString filename = QFileDialog::getSaveFileName(this,"保存文件",QDir::currentPath());
    saveFileName = filename;
    if(filename.isEmpty())
    {
        QMessageBox::information(this,"保存文件失败","请选择一个纯文本文件或者写入一个正确的文件名");
        return ;
    }

    QFile *file = new QFile();
    file->setFileName(filename);
    bool ok = file->open(QIODevice::WriteOnly);
    if(ok)
    {
        QTextStream out(file);
        out << ui->textEdit->toPlainText();
        file->close();
        delete file;
    }
    else
    {
        QMessageBox::information(this,"另存文件失败","另存失败"+file->errorString());
        return ;
    }
    return ;
}
//保存一个文件
void MainWindow::saveFileSlot()
{
    if(saveFileName.isEmpty())
    {
        this->saveAsFileSlot();
    }
    else
    {
        QFile *file = new QFile();
        file->setFileName(saveFileName);
        bool ok = file->open(QIODevice::WriteOnly);
        if(ok)
        {
            QTextStream out(file);
            out << ui->textEdit->toPlainText();
            file->close();
            //qt的core模块,最后需要自己释放
            delete file;
        }
        else
        {
            QMessageBox::information(this,"保存文件","保存失败"+file->errorString());
            return ;
        }
    }
    return ;
}

//设置颜色
void MainWindow::setColorSlot()
{
    const QColor color = QColorDialog::getColor(Qt::green, this);
    if (color.isValid()) {
        ui->textEdit->setTextColor(color);
    }
    else
    {
        QMessageBox::information(this,"设置颜色","设置颜色出错");
        return ;
    }
    return ;
}

//设置字体

void MainWindow::setFontSlot()
{
    bool ok;
    QFont font = QFontDialog::getFont(&ok,  QFont("Helvetica [Cronyx]"), this);
    if (ok) {
        ui->textEdit->setFont(font);
    } else {
        QMessageBox::information(this,"设置字体","字体设置失败");
        return ;
    }
    return ;
}

//插入当前系统时间
void MainWindow::currentDateTimeSlot()
{
    QDateTime dateTime = QDateTime::currentDateTime();
    if(dateTime.isValid())
    {
        QString time = dateTime.toString();
        ui->textEdit->append(time);
    }
    else
    {
        QMessageBox::information(this,"获得系统时间","获得系统当前时间失败");
        return ;
    }
    return ;
}

//关于我
void MainWindow::aboutMeSlot()
{
    QUrl *url = new QUrl("http://blog.leanote.com/post/476941913@qq.com/9428e277d7c1");
    QDesktopServices::openUrl(*url);
    //非qt的gui模块,最后需要自己释放.
    delete url;
    return ;
}
//关于程序
void MainWindow::aboutProgramSlot()
{
    about *dialog = new about();
    dialog->show();
    return ;
}

//程序关闭事件
void MainWindow::closeEvent(QCloseEvent *event)
{
    if(ui->textEdit->document()->isModified())
    {
        QMessageBox msgBox;
        msgBox.setText("文本已经被编辑.");
        msgBox.setInformativeText("想要去保存吗?");
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Save);
        int ret = msgBox.exec();
        switch (ret) {
          case QMessageBox::Save:
              this->saveFileSlot();
              break;
          case QMessageBox::Discard:
              event->accept();
              break;
          case QMessageBox::Cancel:
              event->ignore();
              break;
          default:
              event->ignore();
              break;
        }
    }
    else
    {
        event->accept();
    }
}

