#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QFont>
#include <QFontDialog>
#include <QColor>
#include <QColorDialog>
#include <QDateTime>
#include <QHBoxLayout>
#include <QUrl>
#include <QDesktopServices>
#include <QSplashScreen>
#include <QPixmap>
#include <QTest>
#include "about.h"
#include <QCloseEvent>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public:
    QString saveFileName;

private slots:
    //点击 新建 触发的槽
    void newUntitleFileSlot();
    //点击 打开 触发的槽
    void openFileSlot();
    //点击 另存 触发的槽
    void saveAsFileSlot();
    //点击 保存 触发的槽
    void saveFileSlot();

    //编辑 颜色 触发的槽
    void setColorSlot();
    //编辑 字体 触发的槽
    void setFontSlot();

    //编辑 插入当前系统时间
    void currentDateTimeSlot();

    //帮助 关于我
    void aboutMeSlot();
    //帮助 关于程序
    void aboutProgramSlot();


protected:
    void closeEvent(QCloseEvent *event);


private:
    Ui::MainWindow *ui;

};

#endif // MAINWINDOW_H
